package com.example.demo.service;

import com.example.demo.digitounico.DigitoUnico;
import com.example.demo.model.ResultadoDigitoUnicoDTO;
import com.example.demo.model.Usuario;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
class ResultadoDigitoUnicoServiceTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ResultadoDigitoUnicoService resultadoDigitoUnicoService;


    @Test
    void obterResultadoDigitoUnicoExistente() {
        int digitoUnico = DigitoUnico.obterDigitoUnico("9875", 4);
        ResultadoDigitoUnicoDTO r = new ResultadoDigitoUnicoDTO("9875", 4, 8);
        when(resultadoDigitoUnicoService.obterResultadoDigitoUnicoExistente("9875", 4)).thenReturn(Optional.of(r));

        Optional<ResultadoDigitoUnicoDTO> digitoUnicoExistente = resultadoDigitoUnicoService.obterResultadoDigitoUnicoExistente("9875", 4);
        assertEquals(digitoUnico, digitoUnicoExistente.get().getResultado());
    }

    @Test
    void obterResultadoDigitoUnicoNaoExistente() {
        Optional<ResultadoDigitoUnicoDTO> r = Optional.empty();
        when(resultadoDigitoUnicoService.obterResultadoDigitoUnicoExistente("9875", 4)).thenReturn(r);

        Optional<ResultadoDigitoUnicoDTO> digitoUnicoExistente = resultadoDigitoUnicoService.obterResultadoDigitoUnicoExistente("9875", 4);
        assertTrue(digitoUnicoExistente.isEmpty());
    }

    @Test
    void obterResultadosDigitoUnicoPorUsuario() {
        List<ResultadoDigitoUnicoDTO> resultadosDigitoUnicoDTOretorno = new ArrayList<>();
        Usuario usuario = new Usuario("Joao", "joao@gmail.com");
        resultadosDigitoUnicoDTOretorno.add(new ResultadoDigitoUnicoDTO("1", 1, 1, usuario));
        resultadosDigitoUnicoDTOretorno.add(new ResultadoDigitoUnicoDTO("9875", 4, 8, usuario));

        when(resultadoDigitoUnicoService.obterResultadosDigitoUnicoPorUsuario(1)).thenReturn(resultadosDigitoUnicoDTOretorno);
        List<ResultadoDigitoUnicoDTO> resultadosDigitoUnicoDTO = resultadoDigitoUnicoService.obterResultadosDigitoUnicoPorUsuario(1);

        assertTrue(resultadosDigitoUnicoDTO.size() == 2);
        assertEquals(resultadosDigitoUnicoDTOretorno.get(0).getResultado(), 1);
        assertEquals(resultadosDigitoUnicoDTOretorno.get(1).getResultado(), 8);

    }
}
