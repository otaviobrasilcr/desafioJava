package com.example.demo.digitounico;

import org.assertj.core.error.AssertionErrorFactory;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import static org.junit.jupiter.api.Assertions.*;

class DigitoUnicoTest {

    @Test
    void obterDigitoUnicoDeVariosDigitos() {
        int digitoUnico = DigitoUnico.obterDigitoUnico("9875", 4);
        assertEquals(8, digitoUnico);
    }

    @Test
    void obterDigitoUnicoDeUmDigito() {
        int digitoUnico = DigitoUnico.obterDigitoUnico("1", 0);
        assertEquals(1, digitoUnico);
    }

    @Test
    void obterDigitoUnicoSalvoEmCache() {
        int digitoUnico = DigitoUnico.obterDigitoUnico("9875", 4);
        assertEquals(1, DigitoUnico.ultimosCalculosRealizados.size());
        assertEquals(8, DigitoUnico.ultimosCalculosRealizados.get(0).getResultado());
    }

    @Test
    void validarTamanhoMaximoDe10ElementosNaListaDeResultadosEmCache() {
        DigitoUnico.obterDigitoUnico("1", 1);
        DigitoUnico.obterDigitoUnico("1", 2);
        DigitoUnico.obterDigitoUnico("3", 4);
        DigitoUnico.obterDigitoUnico("10", 2);
        DigitoUnico.obterDigitoUnico("20", 2);
        DigitoUnico.obterDigitoUnico("9875", 4);
        DigitoUnico.obterDigitoUnico("876", 2);
        DigitoUnico.obterDigitoUnico("988", 5);
        DigitoUnico.obterDigitoUnico("323", 1);
        DigitoUnico.obterDigitoUnico("232", 4);
        DigitoUnico.obterDigitoUnico("11", 4);

        assertEquals(10,DigitoUnico.ultimosCalculosRealizados.size());
    }
}