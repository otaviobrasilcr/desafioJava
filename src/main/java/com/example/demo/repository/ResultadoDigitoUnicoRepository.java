package com.example.demo.repository;

import com.example.demo.model.ResultadoDigitoUnico;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ResultadoDigitoUnicoRepository extends CrudRepository<ResultadoDigitoUnico, Integer> {
    @Query("SELECT resultado FROM ResultadoDigitoUnico resultado  WHERE resultado.usuario.id = (:id)")
    List<ResultadoDigitoUnico> obterResultadosDigitoUnicoPorUsuario(@Param("id") int id);
}
