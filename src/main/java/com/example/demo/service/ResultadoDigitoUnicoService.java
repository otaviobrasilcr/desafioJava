package com.example.demo.service;

import com.example.demo.digitounico.DigitoUnico;
import com.example.demo.model.ResultadoDigitoUnico;
import com.example.demo.model.ResultadoDigitoUnicoDTO;
import com.example.demo.model.Usuario;
import com.example.demo.repository.ResultadoDigitoUnicoRepository;
import com.example.demo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResultadoDigitoUnicoService {


    @Autowired
    ResultadoDigitoUnicoRepository resultadoDigitoUnicoRepository;
    @Autowired
    UsuarioRepository usuarioRepository;

    public List<ResultadoDigitoUnicoDTO> obterResultadosDigitoUnicoPorUsuario(int id) {

        List<ResultadoDigitoUnico> resultadosDigitoUnico = resultadoDigitoUnicoRepository.obterResultadosDigitoUnicoPorUsuario(id);
        List<ResultadoDigitoUnicoDTO> resultadosDigitoUnicoDTO = resultadosDigitoUnico.stream()
                .map(resultadoDigitoUnico -> new ResultadoDigitoUnicoDTO(resultadoDigitoUnico.getParametroN(), resultadoDigitoUnico.getParametroK(), resultadoDigitoUnico.getResultado(), resultadoDigitoUnico.getUsuario()))
                .collect(Collectors.toList());
        return resultadosDigitoUnicoDTO;

    }

    public int calcularDigitoUnico(String n, int k, int idUsuario) {
        Optional<ResultadoDigitoUnicoDTO> resultadoDigitoUnicoEmCache = obterResultadoDigitoUnicoExistente(n, k);

        ResultadoDigitoUnico resultadoDigitoUnico = new ResultadoDigitoUnico();
        int digitoUnico = resultadoDigitoUnicoEmCache.map(ResultadoDigitoUnicoDTO::getResultado).orElseGet(() -> DigitoUnico.obterDigitoUnico(n, k));

        if (idUsuario != 0) {
            Optional<Usuario> usuario = usuarioRepository.findById(idUsuario);
            if (usuario.isPresent()) {
                resultadoDigitoUnico.setUsuario(usuario.get());
                resultadoDigitoUnico.setParametroK(k);
                resultadoDigitoUnico.setParametroN(n);
                resultadoDigitoUnico.setResultado(digitoUnico);
                resultadoDigitoUnicoRepository.save(resultadoDigitoUnico);

            }
        }
        return digitoUnico;

    }

    public Optional<ResultadoDigitoUnicoDTO> obterResultadoDigitoUnicoExistente(String n, int k) {
        return DigitoUnico.ultimosCalculosRealizados.stream()
                .filter(r -> r.getN().equals(n) && r.getK() == k)
                .findAny();
    }
}
