package com.example.demo.digitounico;


import com.example.demo.model.ResultadoDigitoUnico;
import com.example.demo.model.ResultadoDigitoUnicoDTO;

import java.util.ArrayList;
import java.util.List;

public class DigitoUnico {
    //lista estática para manter os últimos cálculos de dígito realizados em cache.
    public static List<ResultadoDigitoUnicoDTO> ultimosCalculosRealizados = new ArrayList<>();

    public static int obterDigitoUnico(String n, int k) {
        int digito = 0;
        String p = "";
        int somaCaracteresDeP = 0;

        for (int i = 0; i < k; i++) {
            p = p.concat(n);
        }

        for (int i = 0; i < p.length(); i++) {
            somaCaracteresDeP += Character.getNumericValue(p.charAt(i));
        }

        if (String.valueOf(somaCaracteresDeP).length() == 1) {
            digito = Integer.parseInt(n);
        } else {
            for (int i = 0; i < String.valueOf(somaCaracteresDeP).length(); i++) {
                digito += Character.getNumericValue(String.valueOf(somaCaracteresDeP).charAt(i));
            }
        }

        if (ultimosCalculosRealizados.size() == 10) {
            ultimosCalculosRealizados.remove(0);
        }
        ultimosCalculosRealizados.add(new ResultadoDigitoUnicoDTO(n, k, digito));

        return digito;
    }
}
