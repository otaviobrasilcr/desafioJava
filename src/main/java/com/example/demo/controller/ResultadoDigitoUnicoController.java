package com.example.demo.controller;

import com.example.demo.model.ResultadoDigitoUnicoDTO;
import com.example.demo.service.ResultadoDigitoUnicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ResultadoDigitoUnicoController {

    @Autowired
    ResultadoDigitoUnicoService resultadoDigitoUnicoService;

    @GetMapping("/resultadoDigitoUnico/{id}")
    private List<ResultadoDigitoUnicoDTO> obterResultadosDigitoUnicoPorUsuario(@PathVariable("id") int id) {
        return resultadoDigitoUnicoService.obterResultadosDigitoUnicoPorUsuario(id);
    }

    @GetMapping("/resultadoDigitoUnico/{n}/{k}/{idUsuario}")
    private int calcularDigitoUnico(@PathVariable("n") String n, @PathVariable("k") int k, @PathVariable("idUsuario") int idUsuario) {
        return resultadoDigitoUnicoService.calcularDigitoUnico(n, k, idUsuario);
    }
}
