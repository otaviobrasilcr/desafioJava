package com.example.demo.model;

public class ResultadoDigitoUnicoDTO {

    private String n;
    private int k;
    private int resultado;
    private Usuario usuario;

    public ResultadoDigitoUnicoDTO(String n, int k, int digito, Usuario usuario) {
        this.n = n;
        this.k = k;
        this.resultado = digito;
        this.usuario = usuario;
    }
    public ResultadoDigitoUnicoDTO(String n, int k, int digito) {
        this.n = n;
        this.k = k;
        this.resultado = digito;
    }

    public ResultadoDigitoUnico transformaParaObjeto() {
        return new ResultadoDigitoUnico(n, k,resultado,usuario);
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
