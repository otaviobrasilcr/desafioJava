package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table
public class ResultadoDigitoUnico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String parametroN;
    @Column
    private int parametroK;
    @Column
    private int resultado;
    @ManyToOne
    @JoinColumn
    private Usuario usuario;

    public ResultadoDigitoUnico() {
    }

    public ResultadoDigitoUnico(String n, int k, int resultado, Usuario usuario) {
        this.parametroN = n;
        this.parametroK = k;
        this.resultado = resultado;
        this.usuario = usuario;
    }

    public String getParametroN() {
        return parametroN;
    }

    public void setParametroN(String parametroN) {
        this.parametroN = parametroN;
    }

    public int getParametroK() {
        return parametroK;
    }

    public void setParametroK(int parametroK) {
        this.parametroK = parametroK;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }


}
