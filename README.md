1. Como compilar e executar a aplicação: No terminal de comando, estando no diretório raiz do projeto, rodar o comando :
mvn spring-boot:run
2. Como executar os testes unitários:No terminal de comando, estando no diretório raiz do projeto, rodar o comando :
mvn clean test
3. Swagger:Acessar o link http://localhost:8080/swagger-ui.html#/ após rodar a aplicação.